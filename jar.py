import sys
import platform
import pathlib as pathLib
import zipfile as zipFile
import subprocess
import struct
from tabulate import tabulate #pep / anaconda tabulate pck

JVM_NAMES = {45: '1.1',
             46: '1.2',
             47: '1.3',
             48: '1.4',
             49: '5.0',
             50: '6.0',
             51: '7',
             52: '8',
             53: '9',
             54: '10',
             55: '11',
             56: '12',
             57: '13',
             58: '14',
             }

class ClassFileParser:
    MAGIC_NUMBERS = bytes([int(mv, 0) for mv in ['0xCA', '0xFE', '0xBA', '0xBE']])

    def __init__(self, classFile):
        self.classFile = classFile
        self.classFileIter = iter(classFile)
        self.decode()

    def read_byte(self):
        return bytes([next(self.classFileIter)])

    def read_bytes(self, count):
        return bytes([next(self.classFileIter) for _ in range(count)])

    def unpack(self, fmt, size=1):
        if size == 1:
            data = self.read_byte()
        else:
            data = self.read_bytes(size)
        return struct.unpack(fmt, data)[0]

    def read_uint16(self):
        return self.unpack('>H', 2)

    def decode(self):
        mn = self.read_bytes(4)
        if mn != self.MAGIC_NUMBERS:
            raise Exception('magic numbers %s do not match %s' % (self.MAGIC_NUMBERS, mn))
        self.minor_version = self.read_uint16()
        self.major_version = self.read_uint16()

    def getMinorVersion(self):
        return self.minor_version

    def getMajorVersion(self):
        return self.major_version

class JarStats:
    def __init__(self, jarFile, jdk):
        self.jarFile = jarFile
        self.jdk = jdk

    def getJarFile(self):
        return self.jarFile

    def getJdk(self):
        return self.jdk


class JarCollector:
    def __init__(self, path, ignoreDirs = []):
        self.path = path
        self.jarsWithVersions = []
        self.ignoreDirs = ignoreDirs
        self.findJars(path)

    def findJars(self, path):
        path = pathLib.Path(path)
        if path.is_dir():
            for dir in path.iterdir():
                if self.checkIsDirectory(dir):
                    self.findJars(dir)
                elif self.checkIsFileIsJar(dir):
                    self.getFirstClassInJar(dir)
        elif self.checkIsFileIsJar(path):
            self.getFirstClassInJar(path)

    def getFirstClassInJar(self, fileName):
        with zipFile.ZipFile(fileName, 'r') as jarFile:
            for zi in jarFile.infolist():
                name = zi.filename
                if(len(self.ignoreDirs) > 0 and any(name.startswith(s) for s in self.ignoreDirs)):
                    continue
                if name.endswith('.class'):
                    self.determineJdkVersion(jarFile, name)
                    break

    def determineJdkVersion(self, jarFile, name):
        classFileParser = ClassFileParser(jarFile.read(name))
        self.jarsWithVersions.append(JarStats(jarFile.filename, self.version2string(classFileParser.getMajorVersion())))

    def version2string(self, version):
        return JVM_NAMES[version]

    def checkIsFileIsJar(self, filepath):
        if filepath.exists() and filepath.is_file():
            filepath = pathLib.PurePath(filepath)
            return filepath.suffix == ".jar"

    def checkIsDirectory(self, path):
        return path.exists() and path.is_dir()

    def printJars(self):
        for jar in self.jarsWithVersions:
            print("[JDK {}] {}".format(jar.getVersion(), jar.getFileName()))

    def printJarsAsTable(self):
        table = []
        for jar in self.jarsWithVersions:
            table.append(jar.__dict__)
        print(tabulate(table, headers="keys", tablefmt="pretty"))

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print ("Provide a jar or search directory for jars")
    else:
        jarCollector = JarCollector(sys.argv[1], ["org/springframework/"])
        jarCollector.printJarsAsTable()